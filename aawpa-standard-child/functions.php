<?php 

/**
* add the "standard" theme stylesheet
* @since 0.2
* @author Russell Fair
*/
function aawpa_enqueue_styles(){

	wp_dequeue_style( 'standard' ); //remove standard standard stylesheet

	wp_enqueue_style( 'standard-parental' , get_template_directory_uri() . '/style.css', array(), '0245');

}
add_action('wp_print_styles', 'aawpa_enqueue_styles');
